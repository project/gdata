<?php
/**
 * Copyright and License info
 * 
 * Copyright 2006 Sumit Datta | sumitdatta _at_ gmail _dot_ com
 * 
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

function blog_gdata($op, $path = '') {
  switch ($op) {
    case 'read':
        if (0 == ($module_details = variable_get('gdata_blog', 0))) {
        die(_gdata_error('error'));
        }
        _gdata_blog_feed($path);
      break;

    case 'write':
      break;

    case 'update':
      break;

    case 'delete':
      break;

    case 'search':
      break;

/*
* Return an array cantaining three elements 'implements', 'path', 'help':
*   'implements' :: this is an array of the terms :
*     'read', 'write', 'update', 'delete', 'search', in any order. Each
*     term signifies that your module implements the feature. The
*     absence of any term means that feature is not implemented. If no
*     feature is implemented then the module will NOT be used.
*
*   'path' :: this is the path under /gfeed/ that you want to handle.
*     Do NOT register this path with the normal node_menu( ) hook.
*     Also the administrator may change the path. All that is handled by
*     gdata module. If this is not provided then the name of module is used.
*
*   'help' :: this is a string which explains what your module does (incase
*      the administrator has forgotten it or is confused :) )
*/
    case 'info':
      $array = array(
        'enabled' => 'true',
        'implements' => array('read'),
        'path' => 'blog',
        'help' => t('This is just a testing of GData hook for the blog module'),
      );
      break;
  }
  return $array;
}

/**
 * Helper function for printing blogs
 */
function _gdata_blog_feed($path) {
  global $base_url;
  if(strpos(arg(3), 'gdata') === 0 || strpos(arg(3), 'feed') === 0) {
    die(drupal_not_found());
  }
  $nodes = db_query_range("SELECT n.nid FROM {node} n WHERE n.type = 'blog' AND n.status = '1' ORDER BY n.created DESC", 0, variable_get('atom_feed_entries', 15));

  $feed_info = array();
  $feed_info['title']    = strip_tags(sprintf(t('%s blogs'), variable_get('site_name', 'Drupal')));
  $feed_info['subtitle'] = strip_tags(variable_get('site_slogan', ''));
  $feed_info['path'] = $path;
  _gdata_print_feed($nodes, $feed_info);
}

function _gdata_user_blog_feed() {
  global $path;
  if(strpos(arg(4), 'gdata') === 0 || strpos(arg(4), 'feed') === 0) {
    die(drupal_not_found());
  }
  $user_result = db_query_range("SELECT u.name FROM {users} u WHERE u.uid = %d", arg(1), 0, 1);
  if(!$user = db_result($user_result)) {
    return drupal_not_found();
  }

  $nodes = db_query_range("SELECT n.nid FROM {node} n WHERE n.type = 'blog' AND n.uid = %d AND n.status = '1' ORDER BY n.created DESC", arg(1), 0, variable_get('atom_feed_entries', 15));

  $feed_info = array();
  $feed_info['title']    = sprintf(t("%s's blog"), $user);
  $feed_info['html_url'] = $path;//url('gdata/blog/'. arg(1), NULL, NULL, true);
  $feed_info['gdata_url'] = url(
  (substr($path, 0, 3) == '?q=')?substr($path, 2):$path . arg(1) .'',
  NULL, NULL, true);
  _gdata_print_feed($nodes, $feed_info);
}