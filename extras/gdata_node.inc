<?php
/**
 * Copyright and License info
 * 
 * Copyright 2006 Sumit Datta | sumitdatta _at_ gmail _dot_ com
 * 
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

function node_gdata($op, $path = '') {
  global $base_url, $user;
  switch ($op) {
    case 'read':
      if (0 == ($module_details = variable_get('gdata_node', 0))) {
        die(_gdata_error('error'));
      }
      $sql = "SELECT n.nid FROM {node} n WHERE n.promote = '1' AND n.status = '1' ORDER BY n.created DESC";
      if (false !== ($nodes = db_query_range($sql, 0, variable_get('gdata_feed_entries', 15)))) {
        if (0 != db_num_rows($nodes)) {
          $feed_info = array();
          $feed_info['title']    = strip_tags(variable_get('site_name', 'Drupal'));
          $feed_info['subtitle'] = strip_tags(variable_get('site_slogan', ''));
          $feed_info['path'] = $path;
          _gdata_print_feed($nodes, $feed_info);
        }
        else {
          _gdata_blank_feed($path);
        }
      }
      break;

    case 'write':
      break;

    case 'update':
      break;

    case 'delete':
      break;

    case 'search':
      break;

/*
* Return an array cantaining three elements 'implements', 'path', 'help':
*   'implements' :: this is an array of the terms :
*     'read', 'write', 'update', 'delete', 'search', in any order. Each
*     term signifies that your module implements the feature. The
*     absence of any term means that feature is not implemented. If no
*     feature is implemented then the module will NOT be used.
*
*   'path' :: this is the path under /gfeed/ that you want to handle.
*     Do NOT register this path with the normal node_menu( ) hook.
*     Also the administrator may change the path. All that is handled by
*     gdata module. If this is not provided then the name of module is used.
*
*   'help' :: this is a string which explains what your module does (incase
*      the administrator has forgotten it or is confused :) )
*/
    case 'info':
      $array = array(
        'enabled' => 'true',
        'implements' => array('read'),
        'path' => 'node',
        'help' => t('This is an implementation of syndication through GData for node data'),
      );
      break;
  }
  return $array;
}
