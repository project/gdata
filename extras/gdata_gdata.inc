<?php
/**
 * Copyright and License info
 * 
 * Copyright 2006 Sumit Datta | sumitdatta _at_ gmail _dot_ com
 * 
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  */

function gdata_gdata($op, $path = '') {
  global $base_url, $user;
  switch ($op) {
    case 'read':
      if (0 == ($module_details = variable_get('gdata_gdata', 0))) {
        die(_gdata_error('error'));
      }
      $sql = 'SELECT * from {myfeed}';
      if (false !== ($nodes = db_query($sql))) {
        if (0 != db_num_rows($nodes)) {
          _gdata_gdata_feed($nodes, $path);
        }
        else {
          _gdata_blank_feed($path);
        }
      }
      else {
      }
      break;

    case 'write':
      $request = _gdata_xml_request();
      db_query('INSERT INTO {myfeed} '.
        '(id, ver, title, content, author_name, author_email, created, updated)'.
        "VALUES(".($id = db_next_id('{myfeed}_id')).",".
        "1,".
        "'".$request['title']['value']."',".
        "'".$request['content']['value']."',".
        "'".$request['author']['name']."',".
        "'".$request['author']['email']."',".
        ($time = time()).",".
        $time.")");
      $output = '<?xml version="1.0" encoding="utf-8"?>'."\n";
      $output .= '<entry xmlns="http://www.w3.org/2005/Atom">'."\n";
      $output .= '  <id>'.$id.'</id>'."\n";
      $output .= '  <link rel="edit" href="'.url('gdata/myfeed/'.$id.'/1', NULL, NULL, TRUE).'"/>'."\n";
      $output .= '  <updated>'._gdata_timestamp2w3dtf($time).'</updated>'."\n";
      $output .= '  <author>'."\n";
      $output .= '    <name>'.$request['author']['name'].'</name>'."\n";
      $output .= '    <email>'.$request['author']['email'].'</email>'."\n";
      $output .= '  </author>'."\n";
      $output .= '  <title type="'.$request['title']['type'].'">'.$request['title']['value'].'</title>'."\n";
      $output .= '  <content type="'.$request['content']['type'].'">'.$request['content']['value'].'</content>'."\n";
      $output .= '</entry>'."\n";
      drupal_set_header('Content-type: application/xml');
      drupal_set_header('HTTP/1.1 201 CREATED');
      print $output;
      break;

    case 'update':
      break;

    case 'delete':
      drupal_set_header('Content-type: text/plain');
      $request = explode('/', $path);
      if (count($request) == 4) {
        if (is_int(intval($request[2])) && is_int(intval($request[3]))) {
          $qr = db_query('SELECT * FROM {myfeed} WHERE id='.$request[2]);
          if (db_num_rows($qr) == 0) {
            drupal_set_header('HTTP/1.1 404 NOT FOUND');
            print 'The requested data does not exist. Delete operation un-successful.';
          }
          else {
          	db_query('DELETE FROM {myfeed} WHERE id='.$request[2]);
          	print db_affected_rows().' record(s) deleted. Delete operation successful.';
          }
        }
        else {
          drupal_set_header('HTTP/1.1 400 BAD REQUEST');
          print 'Delete request invalid.';
        }
      }
      else {
      	drupal_set_header('HTTP/1.1 400 BAD REQUEST');
        print 'Delete request invalid.';
      }
      break;

    case 'search':
      break;

/*
* Return an array cantaining four elements 'enabled', 'implements', 'path', 'help':
*   'enabled' :: this tells if this gdata implementation is to work. Required.
*   
*   'implements' :: this is an array of the terms :
*     'read', 'write', 'update', 'delete', 'search', in any order. Each
*     term signifies that your module implements the feature. The
*     absence of any term means that feature is not implemented. If no
*     feature is implemented then the module will NOT be used.
*
*   'path' :: this is the path under /gfeed/ that you want to handle.
*     Do NOT register this path with the normal node_menu( ) hook.
*     Also the administrator may change the path. All that is handled by
*     gdata module. If this is not provided then the name of module is used.
*
*   'help' :: this is a string which explains what your module does (incase
*      the administrator has forgotten it or is confused :) )
*/
    case 'info':
      $array = array(
        'enabled' => 'true',
        'implements' => array('read', 'write','delete'),
        'path' => 'myfeed',
        'help' => t('This is just a testing of GData hook'),
      );
      return $array;
      break;
  }
}

function _gdata_gdata_feed($nodes, $path) {
  drupal_set_header('Content-type: application/xml');
  drupal_set_header('HTTP/1.1 200 OK');
  $output =
'<?xml version="1.0" encoding="utf-8"?>
  <feed xmlns="http://www.w3.org/2005/Atom">
  <title>'.variable_get('site_name', 'Drupal').'</title>
  <updated>'._gdata_timestamp2w3dtf(time()).'</updated>
  <id>'.url($path, NULL, NULL, TRUE).'</id>
  <link href="'.url($path, NULL, NULL, TRUE).'" rel="self"/>';
  while ($node = db_fetch_array($nodes)) {
  	$output .= '
  <entry>
    <id>'.$node['id'].'</id>
    <link rel="edit" href="'.url($path, NULL, NULL, TRUE).$node['id'].'/'.$node['ver'].'"/>
    <updated>'._gdata_timestamp2w3dtf($node['updated']).'</updated>
    <author>';
    if ($node['author_name'] == '') {
    	$output .= '
    	<name/>';
    }
    else {
    	$output .= '
      <name>'.$node['author_name'].'</name>';
    }
    if ($node['author_email'] == '') {
    	$output .= '
      <email/>';
    }
    else {
    	$output .= '
      <email>'.$node['author_email'].'</email>';
    }
    $output .= '
    </author>
    <title type="text">'.$node['title'].'</title>
    <content type="text">'.$node['content'].'</content>
  </entry>';
  }
  $output .= '
</feed>';
  print $output;
}
