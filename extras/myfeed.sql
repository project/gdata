CREATE TABLE myfeed (
  id int(10) unsigned NOT NULL default '0',
  ver int(10) unsigned NOT NULL default '0',
  title varchar(255) NOT NULL default '',
  content text,
  author_name varchar(255) NOT NULL default '',
  author_email varchar(255) NOT NULL default '',
  created int(11),
  updated int(11),
  PRIMARY KEY (id, ver)
);
